#ifndef CARTE_H
#define CARTE_H
#include <string>
using namespace std;


class Carte
{
    public:
        Carte(int attaque, int defense, int cout_mana, string nom,bool compteur);
        virtual ~Carte();
        void afficherCarte();
        int getAttaque();
        int getDefense();
        int getCout();
        bool getCompteur();
        string getNom();
        void setAtk(int);
        void setDef(int);
        void setDefafterAtk(int);
        void setCompteur(bool);

    protected:

    private:
        int attaque;
        int defense;
        int cout_mana;
        string nom;
        bool compteur;
};

class Guerrier: public Carte
{
    public :
        Guerrier(int attaque, int defense, int cout_mana, string nom,bool compteur):Carte(attaque,defense,cout_mana,nom,compteur){
        }

    private :
};

class Magicien : public Carte
{
    public :
        Magicien(int attaque, int defense, int cout_mana, string nom,bool compteur):Carte(attaque,defense,cout_mana,nom,compteur){
        }
    private :
};


class Paladin: public Carte
{
    public :
        Paladin(int attaque, int defense, int cout_mana, string nom,bool compteur):Carte(attaque,defense,cout_mana,nom,compteur){
        }
    private :
};


class Demoniste: public Carte
{
    public :
        Demoniste(int attaque, int defense, int cout_mana, string nom,bool compteur):Carte(attaque,defense,cout_mana,nom,compteur){
        }
    private :
};

class Petizetre: public Carte
{
    public :
        Petizetre(int attaque, int defense, int cout_mana, string nom,bool compteur):Carte(attaque,defense,cout_mana,nom,compteur){
        }
    private :
};

#endif // CARTE_H
