#ifndef POTIONDEF_H
#define POTIONDEF_H
#include <string>
using namespace std;

class PotionDef
{
    public:
        PotionDef(string nom, int cout_mana, int def);
        virtual ~PotionDef();
        int getMana();
        int getDef();
        int getCout();
        string getNom();

    protected:

    private:
        string nom;
        int cout_mana;
        int def;
};

#endif // POTIONDEF_H
