#ifndef POTIONATK_H
#define POTIONATK_H
#include <string>
using namespace std;


class PotionAtk
{
    public:
        PotionAtk(string nom, int cout_mana, int atk);
        virtual ~PotionAtk();
        int getMana();
        int getAtk();
        int getCout();
        string getNom();

    protected:

    private:
        string nom;
        int cout_mana;
        int atk;
};

#endif // POTIONATK_H
