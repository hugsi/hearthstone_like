#ifndef DECK_H
#define DECK_H
#include "Carte.h"
#include <vector>
#include <ctime>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;




class Deck
{
    public:
        Deck();
        virtual ~Deck();
        void chargerDeck_Monstre();
        void afficherDeck();
        void melangeDeck();
        Carte* prendreCarte();
        Carte* prendreCarteChoix(int x);
        Carte* selectCarteChoix(int x);
        void ajouterCarte(Carte* c);
        int getSize();
        bool isEmpty();

    protected:

    private:
        vector <Carte*> deck;
};

#endif // DECK_H
