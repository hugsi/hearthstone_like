#ifndef JEU_H
#define JEU_H
#include "Deck.h"
#include "Carte.h"
#include "Joueur.h"
#include "Personnage.h"



class Jeu
{
    public:
        Jeu();
        virtual ~Jeu();
        void afficherMenu();
        void commencerJeu();
        void choisirPerso(Joueur*);
        void DistribuerDebut(Joueur*);
        void choisirCarte(Joueur*, Deck*);
        void initDeck(Joueur*,Deck*);
        void Plateau(Joueur*, Joueur*,int);
        void menuAction(Joueur*,Deck*,Personnage*,Deck*,Deck*,Joueur*,int);
        void menuCombat(Joueur*,Deck*,Personnage*,Deck*,Joueur*,int);
        void choisirPotion(Joueur*,Personnage*,Deck*);
        void resetCompteur(Joueur*);
        void piocherCarte(Joueur*);
    protected:

    private:
        Deck* boardJ1;
        Deck* boardJ2;
        Deck* cimetiere;


};

#endif // JEU_H
