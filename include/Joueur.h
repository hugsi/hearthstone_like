#ifndef JOUEUR_H
#define JOUEUR_H
#include <string>
#include <vector>
#include "Deck.h"
#include "Personnage.h"
#include "Inventaire.h"



class Joueur
{
      public:
        Joueur();
        virtual ~Joueur();
        string getNom();
        Deck* getHand();
        Deck* getPioche();
        Personnage* getPerso();
        Inventaire* getInventaire();
        void afficherJoueur();
        void afficherHand();
        void melangeHand();
        void setPerso(Personnage* perso);
        void appliquerPotionVIE(PotionVie*);
        void appliquerPotionDEF(int,PotionDef*);
        void appliquerPotionATK(int,PotionAtk*);
        void appliquerATKPerso(int);
        Deck* getSize();
        void setNom(string nom);


    protected:

    private:
        string nom;
        Deck* pioche;
        Deck* hand;
        Personnage* perso;
        Inventaire* invent;

};

#endif // JOUEUR_H
