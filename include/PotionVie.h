#ifndef POTIONVIE_H
#define POTIONVIE_H
#include "Personnage.h"
#include <string>
using namespace std;


class PotionVie
{
    public:
        PotionVie(string nom, int cout_mana, int vie);
        virtual ~PotionVie();
        int getMana();
        int getVie();
        int getCout();
        string getNom();

    protected:

    private:
        string nom;
        int cout_mana;
        int vie;
};

#endif // POTIONVIE_H
