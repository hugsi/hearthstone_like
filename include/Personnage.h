#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include <iostream>
#include <string>
using namespace std;


class Personnage
{
    public:
        Personnage(int ptsVie, int mana, string nom);
        void afficherPerso();
        void setVie(int);
        void setMana(int);
        void setManaPlus(int);
        void setManaPlay(int);
        string getNom();
        int getMana();
        int getPtsVie();
        virtual ~Personnage();

    protected:

    private:
        int ptsVie;
        int mana;
        string nom;
};

class Shaman: public Personnage
{
    public :
        Shaman(int ptsVie, int mana, string nom):Personnage(ptsVie,mana,nom){
        }

    private :
};

class Foudroyeur: public Personnage
{
    public :
        Foudroyeur(int ptsVie, int mana, string nom):Personnage(ptsVie,mana,nom){
        }

    private :
};

class Spadassin: public Personnage
{
    public :
        Spadassin(int ptsVie, int mana, string nom):Personnage(ptsVie,mana,nom){
        }

    private :
};

class Berserker: public Personnage
{
    public :
        Berserker(int ptsVie, int mana, string nom):Personnage(ptsVie,mana,nom){
        }

    private :
};

#endif // PERSONNAGE_H
