#ifndef POTION_H
#define POTION_H
#include <string>
using namespace std;

class Potion
{
    public:
        Potion(string nom, int cout_mana);
        virtual ~Potion();
        int getMana();
        string getNom();

    protected:

    private:
        string nom;
        int cout_mana;
};
#endif // POTION_H
