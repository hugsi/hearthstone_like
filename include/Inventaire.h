#ifndef INVENTAIRE_H
#define INVENTAIRE_H
#include "PotionVie.h"
#include "PotionAtk.h"
#include "PotionDef.h"
#include <vector>
using namespace std;

class Inventaire
{
    public:
        Inventaire();
        virtual ~Inventaire();
        void chargerInventaire();
        void afficherListATK();
        void afficherListDEF();
        void afficherListVIE();
        PotionVie* choixPotionVIE(int);
        PotionAtk* choixPotionATK(int);
        PotionDef* choixPotionDEF(int);

    protected:

    private:
        vector <PotionVie*> listPotionVIE;
        vector <PotionAtk*> listPotionATK;
        vector <PotionDef*> listPotionDEF;


};

#endif // INVENTAIRE_H
