#include "Joueur.h"
#include <string>
#include <iostream>
#include "Carte.h"
#include "Deck.h"
#include "Personnage.h"
#include "Inventaire.h"
using namespace std;

Joueur::Joueur()
{
    cout<<"Entrez votre nom soldat : "<<endl;
    cin>>nom;
    this->setNom(nom);
    this->hand = new Deck;
    this->pioche = new Deck;
    this->invent = new Inventaire;
}

Joueur::~Joueur()
{
    //dtor
}

void Joueur::setNom(string nom){
    this->nom = nom;
}

string Joueur::getNom(){
    return this->nom;
}

void Joueur::afficherJoueur()
{
        cout<<this->getNom()<<" possede "<<this->pioche->getSize()<<" cartes dans son paquet"<<endl;
}


void Joueur::afficherHand(){
    this->hand->afficherDeck();
}

Deck* Joueur::getHand(){
    return this->hand;
}

Deck* Joueur::getPioche(){
    return this->pioche;
}

Personnage* Joueur::getPerso(){
    return this->perso;
}

Inventaire* Joueur::getInventaire(){
   return this->invent;
}
void Joueur::setPerso(Personnage* perso){
    this->perso = perso;
}

void Joueur::appliquerPotionVIE(PotionVie* p){
    this->getPerso()->setVie(this->getPerso()->getPtsVie()+p->getVie());
}

void Joueur::appliquerPotionDEF(int c,PotionDef* p){
    this->getHand()->selectCarteChoix(c)->setDef(p->getDef());
}

void Joueur::appliquerPotionATK(int c,PotionAtk* p){
    this->getHand()->selectCarteChoix(c)->setAtk(p->getAtk());
}

void Joueur::appliquerATKPerso(int degats){
    this->getPerso()->setVie(this->getPerso()->getPtsVie()-degats);
}

