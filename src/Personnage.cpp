#include "Personnage.h"
#include <string>

Personnage::Personnage(int ptsVie, int mana, string nom)
{
   this->ptsVie=ptsVie;
   this->mana=mana; //ctor
   this->nom=nom;
}

Personnage::~Personnage()
{
    //dtor
}

int Personnage::getPtsVie(){
    return ptsVie;
};

int Personnage::getMana(){
    return mana;
};

string Personnage::getNom(){
    return nom;
};

void Personnage::setVie(int ptsVie){
    this->ptsVie=ptsVie;
}
void Personnage::setMana(int m){
    this->mana=mana;
}
void Personnage::setManaPlus(int m){
    this->mana=mana+m;
}
void Personnage::setManaPlay(int m){
    this->mana=mana-m;
}
void Personnage::afficherPerso(){
    cout << this->getNom() << " : " <<" VIE: "<< this->getPtsVie() << "     MANA: " <<this->getMana()<<endl;
}
