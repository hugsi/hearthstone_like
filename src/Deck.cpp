#include "Deck.h"
#include "Carte.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

Deck::Deck()
{
    //ctor
}

Deck::~Deck()
{
    //dtor
}

void Deck::chargerDeck_Monstre(){
    int a[8] = {2,3,4,5,6,7,8,9};
    int d[8] = {1,2,3,4,5,6,7,8};
    int c[8] = {2,3,4,5,6,7,8,9};
    string n[5] = {"Magicien","Guerrier","Paladin","Demoniste","Petizetre"};
    int y = 0;
    int v = 1; //Pour cr�er des monstres moins couteux

    for (int l=0;l<1;l++){
        for (int i=0;i<8;i++){
            this->deck.push_back(new Magicien(a[i],d[i],c[i],n[y],false));
        }
        y++;
        for (int i=0;i<8;i++){
            this->deck.push_back(new Guerrier(a[i],d[i],c[i],n[y],false));
        }
        y++;
        for (int i=0;i<8;i++){
            this->deck.push_back(new Paladin(a[i],d[i],c[i],n[y],false));
        }
        y++;
        for (int i=0;i<8;i++){
            this->deck.push_back(new Demoniste(a[i],d[i],c[i],n[y],false));
        }
        y++;
        for (int i=0;i<8;i++){
            this->deck.push_back(new Petizetre(a[v],d[v],c[v],n[y],false));
        }
    }
}

void Deck::afficherDeck(){
    for (int i=0; i< this->deck.size(); i++){
        cout <<"#################################"<<endl;
        cout <<"#################################"<<endl;
        cout <<"## ATK: "<<this->deck[i]->getAttaque()<<"              DEF: "<<this->deck[i]->getDefense()<<"  ##"<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"## NOM: "<<this->deck[i]->getNom()<<"                ##   "<<i+1<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"##                             ##"<<endl;
        cout <<"## MANA : "<<this->deck[i]->getCout()<<"                    ##"<<endl;
        cout <<"#################################"<<endl;
        cout <<"#################################"<<endl;
    }
}

void Deck::melangeDeck(){
    Carte* temp =0;

    for (int i=0; i < this->deck.size(); i++){
        int randomNb = rand() % this->deck.size();
        temp = this->deck[i];
        this->deck[i] = this->deck[randomNb];
        this->deck[randomNb] = temp;
    }
}

Carte* Deck::prendreCarte(){
    Carte* c = this->deck[0];
    this->deck.erase(this->deck.begin());
    return c;
}

Carte* Deck::prendreCarteChoix(int x){
    Carte* c = this->deck[x-1];
    this->deck.erase(this->deck.begin());
    return c;
}
Carte* Deck::selectCarteChoix(int x){
    Carte* c = this->deck[x-1];
    return c;
}
void Deck::ajouterCarte(Carte* c){
    this->deck.push_back(c);
}

int Deck::getSize(){
    return this->deck.size();
}

bool Deck::isEmpty(){
    return this->deck.empty();
}
