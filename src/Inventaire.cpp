#include "Inventaire.h"
#include "PotionVie.h"
#include "Joueur.h"
#include <iostream>
#include <string>
using namespace std;

Inventaire::Inventaire()
{
    //ctor
}

Inventaire::~Inventaire()
{
    //dtor
}

void Inventaire::chargerInventaire(){
    int a[2] = {2,3};
    int d[2] = {2,3};
    int v[2] = {4,5};
    string n[3] = {"Attaque","Defense","Vie"};
    int y = 0;
    for (int i=0;i<1;i++){
        for (int x=0;x<2;x++){
            this->listPotionATK.push_back(new PotionAtk(n[y],0,a[x]));
        }
        y++;
        for (int x=0;x<2;x++){
            this->listPotionDEF.push_back(new PotionDef(n[y],0,d[x]));
        }
        y++;
        for (int x=0;x<2;x++){
            this->listPotionVIE.push_back(new PotionVie(n[y],0,v[x]));
        }
    }
}

void Inventaire::afficherListATK(){
    for(int i=0;i<this->listPotionATK.size();i++){
        cout<<this->listPotionATK[i]->getAtk()<<" points d'attaque supplementaire"<< "[COUT : 0 MANA]"<<endl;
    }
}

void Inventaire::afficherListVIE(){
    for(int i=0;i<this->listPotionVIE.size();i++){
        cout<<this->listPotionVIE[i]->getVie()<<" points de vie supplementaire"<< "[COUT : 0 MANA]"<<endl;
    }
}

void Inventaire::afficherListDEF(){
    for(int i=0;i<this->listPotionDEF.size();i++){
        cout<<this->listPotionDEF[i]->getDef()<<" points de defense supplementaire"<< "[COUT : 0 MANA]"<<endl;
    }
}

PotionVie* Inventaire::choixPotionVIE(int c){
    PotionVie* p = this->listPotionVIE[c-1];
    this->listPotionVIE.erase(this->listPotionVIE.begin());
    return p;
}

PotionAtk* Inventaire::choixPotionATK(int c){
    PotionAtk* p = this->listPotionATK[c-1];
    this->listPotionATK.erase(this->listPotionATK.begin());
    return p;
}

PotionDef* Inventaire::choixPotionDEF(int c){
    PotionDef* p = this->listPotionDEF[c-1];
    this->listPotionDEF.erase(this->listPotionDEF.begin());
    return p;
}



