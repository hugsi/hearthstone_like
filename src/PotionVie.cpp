#include "PotionVie.h"
#include "Personnage.h"

PotionVie::PotionVie(string nom, int cout_mana, int vie)
{
    this->cout_mana=cout_mana;
    this->nom=nom;
    this->vie=vie;
}

PotionVie::~PotionVie()
{
    //dtor
}

int PotionVie::getVie(){
    return this->vie;
}

int PotionVie::getMana(){
    return this->cout_mana;
}

string PotionVie::getNom(){
    return this->nom;
}
int PotionVie::getCout(){
    return this->cout_mana;
}
