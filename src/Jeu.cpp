#include "Jeu.h"
#include "Joueur.h"
#include <string>
#include <iostream>
#include <iomanip>
#include "Carte.h"
#include "Deck.h"
#include "Personnage.h"
using namespace std;
Jeu::Jeu()
{
    this->boardJ1 = new Deck;
    this->boardJ2 = new Deck;
    this->cimetiere = new Deck;
}

Jeu::~Jeu()
{
    //dtor
}
void Jeu::DistribuerDebut(Joueur* j){
    for (int i=0;i<7;i++){
        j->getHand()->ajouterCarte(j->getPioche()->prendreCarte());
    }
}
void Jeu::afficherMenu(){

 int choix;
    bool jeuON = true;
    while (jeuON != false) {


        cout << endl << " 1 - Commencer la partie" << endl;
        cout << " 2 - Regles du jeu " << endl;
        cout << " 3 - Voir les personnages " << endl;
        cout << " 4 - Quittez le jeu" << endl;
        cout << endl << "Choix : ";

        cin >> choix;

        switch (choix)
        {
        case 1:
            cout << "La partie va commencer " << endl;
            jeuON = false;
            system("pause");
            system("cls");
            break;
        case 2:
            cout << endl << " Le but du jeu est simple, faire descendre les points de vie de votre adversaire grace a des monstres poses sur le terrain" << endl;
            cout << endl << " Premierement vous allez devoir choisir un personnage par joueur possedant des points de vie et une barre de mana" <<endl;
            cout << " -- Je vous invite donc a aller regarder les particularites de chaque personnage pour connaitre ses points de mana de depart ainsi que ses points de vie" <<endl;
            cout << endl << " Le jeu se joue uniquement a 2 joueurs et a chaque tour, chaque personnage se verra attribue 3 points de mana supplementaire" << endl;
            cout << endl << " Chaque joueur se voit attribue 20 cartes monstres, ainsi qu'un inventaire contenant des potions" << endl;
            cout << " Les monstres possedent des points d'Attaque et des points de Defense (les points de defense correspondant a leur point de vie)" << endl;
            cout << endl << " Chaque monstre possede un cout, effectivement il vous faudra bien gerer votre barre de mana pour jouer tel ou tel monstre sur le board" << endl;
            cout << " -- Bien entendu les monstres les plus forts sont les plus gourmands en mana" << endl;
            cout << endl <<" Un monstre ne peut pas attaquer directement le personnage adverse si une carte monstre est posee sur le terrain de l'ennemi" << endl;
            cout << endl << " - Chaque monstre attaque une fois par tour" << endl;
            cout <<" -- Si un monstre decide d'attaquer un monstre adverse possedant plus de d�fense que sa propre attaque, il se verra detruit!"<<endl;
            cout <<" --- Cependant le monstre adverse se verra soustraire des points de defense egal aux points d'attaque recu" << endl;
            cout << endl << "Je vous invite donc a etre de fins strateges" << endl;
            cout << endl << "En garde et que le meilleur gagne" << endl;
            break;
        case 3:
            cout << endl << " - Le SHAMAN :" <<endl;
            cout << " -- Possedant un profil classique il commence avec 20 pts de vie mais possede 2 points de mana en debut de partie" <<endl;

            cout << endl << " - Le SPADASSIN :" <<endl;
            cout << " -- Possedant un profil classique il commence avec 20 pts de vie mais possede 2 points de mana en debut de partie" <<endl;

            cout << endl << " - Le FOUDROYEUR :" <<endl;
            cout << " -- Possedant un profil plus agressif il commence avec 10 pts de vie mais possede 4 points de mana en debut de partie" <<endl;

            cout << endl << " - Le BERSERKER :" <<endl;
            cout << " -- Possedant un profil plus d�fensif il commence avec 25 pts de vie mais possede 1 point de mana en debut de partie" <<endl;
            break;
        case 4:
            cout << "Fin du jeu";
            exit(EXIT_FAILURE);
            break;
        default:
            cout << "Veuillez choisir un chiffre entre 1 et 4";
            break;
        }

    }
}
void Jeu::Plateau(Joueur* j1, Joueur* j2,int tour){
cout << "######################################################################################" <<endl;
cout << "#########################          ROUND : "<<tour<<"         #################################" <<endl;
cout << "######################################################################################" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "### "<<j1->getNom()<<" :        || VIE: "<<j1->getPerso()->getPtsVie()<<"   || MANA: "<<j1->getPerso()->getMana()<<setw(45)<<"###" <<endl;
cout << "###                                                                                ###" <<endl;
boardJ1->afficherDeck();
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
boardJ2->afficherDeck();
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "### "<<j2->getNom()<<" :        || VIE: "<<j2->getPerso()->getPtsVie()<<"   || MANA: "<<j2->getPerso()->getMana()<<setw(42)<<"###" <<endl;
cout << "###                                                                                ###" <<endl;
cout << "######################################################################################" <<endl;
cout << "######################################################################################" <<endl;
}
void Jeu::commencerJeu(){
srand(time(NULL));
Deck* leDeck = new Deck;
leDeck->chargerDeck_Monstre();
leDeck->melangeDeck();
Joueur* j1 = new Joueur;
Joueur* j2 = new Joueur;
int tour = 0;
initDeck(j1,leDeck);
initDeck(j2,leDeck);
j1->getInventaire()->chargerInventaire();
j2->getInventaire()->chargerInventaire();
system("cls");
choisirPerso(j1);
system("cls");
choisirPerso(j2);
j1->getPerso()->afficherPerso();
cout << "\n" << endl;
cout << "VS" << endl;
cout << "\n" << endl;
j2->getPerso()->afficherPerso();
DistribuerDebut(j1);
DistribuerDebut(j2);
system("pause");
system("cls");
while(j1->getPerso()->getPtsVie()>=0||j2->getPerso()->getPtsVie()>=0){
tour++;
resetCompteur(j1);
resetCompteur(j2);
menuAction(j1,boardJ1,j1->getPerso(),j1->getHand(),boardJ2,j2,tour);
menuAction(j2,boardJ2,j2->getPerso(),j2->getHand(),boardJ1,j1,tour);
Plateau(j1,j2,tour);
piocherCarte(j1);
piocherCarte(j2);
}
}
void Jeu::piocherCarte(Joueur* j){
j->getHand()->ajouterCarte(j->getPioche()->prendreCarte());
}
void Jeu::resetCompteur(Joueur* j){
    for (int i=1;i<j->getHand()->getSize();i++){
        j->getHand()->selectCarteChoix(i)->setCompteur(false);
}
}
void Jeu::menuCombat(Joueur* j, Deck* d, Personnage* p, Deck* boardennemi, Joueur* joueurEnnemi,int tour){
    int choix;
    bool jeuON = true;
    while (jeuON != false) {

        cout << endl << "Choississez une cible " <<j->getNom()<< endl;
        cout << " 1 - Une carte ennemi ? " << endl;
        cout << " 2 - Son personnage ? (si il n'y a pas de cartes ennemi presentes) " << endl;
        cout << " 3 - Afficher le plateau" << endl;
        cout << " 4 - Terminer la phase de combat!" << endl;
        cout << endl << "Choix : ";
        cin >> choix;

        switch (choix)
        {
        case 1 :
            d->afficherDeck();
            int a;
            int b;
            int degats;
            cout << "Quelle carte voulez vous utilisez pour attaquer?" << endl;
            cin >> a;
            if (d->selectCarteChoix(a)->getCompteur()!=false){

                cout << "Cette carte a deja attaquee" << endl;
                break;
            }
            else if (boardennemi->isEmpty()==true) {

                cout <<"Le board de l'ennemi est vide"<<endl;
                break;
            }
            else

                degats=d->selectCarteChoix(a)->getAttaque();
                system("cls");
                boardennemi->afficherDeck();
                cout << "Quelle carte attaquer?" << endl;
                cin >> b;
                boardennemi->selectCarteChoix(b)->setDefafterAtk(degats);
                d->selectCarteChoix(a)->setDefafterAtk(boardennemi->selectCarteChoix(b)->getAttaque());

                    if(boardennemi->selectCarteChoix(b)->getDefense()<=0){

                        cout<<"Carte detruite et envoyee au cimetiere"<<endl;
                        cimetiere->ajouterCarte(boardennemi->prendreCarteChoix(b));

                        }
                    if(d->selectCarteChoix(a)->getDefense()<=0){
                        cout<<"Votre carte a ete detruite au combat"<<endl;
                        cimetiere->ajouterCarte(d->prendreCarteChoix(a));
                    }
                d->selectCarteChoix(a)->setCompteur(true);
                system("pause");
                system("cls");
                jeuON=false;
                break;

        case 2 :
            if (boardennemi->isEmpty()==false){
                cout << "Le board du joueur adverse n'est pas vide" <<endl;
                break;
            }
                d->afficherDeck();
                cout << "Quelle carte voulez vous utilisez pour attaquer?" << endl;
                cin >> a;
            if (d->selectCarteChoix(a)->getCompteur()!=false){
                cout << "Cette carte a deja attaquee" <<endl;
                break;
            }
            else
                degats=d->selectCarteChoix(a)->getAttaque();
                system("cls");
                cout <<"############## ATTAQUE DIRECT ! ##############" << endl;
                joueurEnnemi->appliquerATKPerso(degats);
                d->selectCarteChoix(a)->setCompteur(true);
                if (joueurEnnemi->getPerso()->getPtsVie()<=0){
                    cout <<"############################"<<j->getNom()<<" remporte le combat! ############################"<<endl;
                    exit(0);
                }
                else
                    cout << endl <<"Le personnage ennemi passe desormais a "<<joueurEnnemi->getPerso()->getPtsVie()<<" points de vie"<<endl;
                    system("pause");
                    system("cls");
                    jeuON=false;
                    break;
        case 3 :
            Plateau(j,joueurEnnemi,tour);
            break;
        case 4 :
            jeuON=false;
            break;
        default:
            cout << "Veuillez choisir un chiffre entre 1 et 4" <<endl;
            break;

        }
    }
}
void Jeu::choisirPotion(Joueur* j, Personnage* p, Deck* h){
     int choix;
    bool jeuON = true;
    while (jeuON != false) {

        cout << endl << "Choississez une potion !" << endl;
        cout << " 1 - Potion de Vie (utilisable sur votre personnage) " << endl;
        cout << " 2 - Potion de Defense (utilisable sur une carte) " << endl;
        cout << " 3 - Potion d'Attaque (utilisable sur une carte)" << endl;
        cout << endl << "Choix : ";

        cin >> choix;

        switch (choix)
        {
        case 1:
            int a;
            j->getInventaire()->afficherListVIE();
            cout <<"\n"<<endl;
            cout <<"Quel potion?"<<endl;
            cin >> a;
            j->appliquerPotionVIE(j->getInventaire()->choixPotionVIE(a));
            cout <<"Votre personnage passe maintenant a "<<j->getPerso()->getPtsVie()<<" de vie"<<endl;
            jeuON = false;
            break;
        case 2:
            int ca;
            j->getInventaire()->afficherListDEF();
            cout <<"\n"<<endl;
            cout <<"Quelle potion?"<<endl;
            cin >> a;
            system("cls");
            j->afficherHand();
            cout <<"Sur quelle carte?"<<endl;
            cin >> ca;
            j->appliquerPotionDEF(ca,j->getInventaire()->choixPotionDEF(a));
            j->afficherHand();
            jeuON = false;
            break;
        case 3:
            j->getInventaire()->afficherListATK();
            cout <<"\n"<<endl;
            cout <<"Quelle potion?"<<endl;
            cin >> a;
            system("cls");
            j->afficherHand();
            cout <<"Sur quelle carte?"<<endl;
            cin >> ca;
            j->appliquerPotionATK(ca,j->getInventaire()->choixPotionATK(a));
            cout <<"\n"<<endl;
            j->afficherHand();
            jeuON = false;
            break;

        default:
            cout << "Veuillez choisir un chiffre entre 1 et 3" <<endl;
            break;
        }

    }
}
void Jeu::menuAction(Joueur* j, Deck* d, Personnage* p, Deck* h, Deck* boardennemi, Joueur* joueurEnnemi,int tour){
    int choix;
    bool jeuON = true;
    while (jeuON != false) {

        cout << endl << "Choississez une action " <<j->getNom()<<" [HP : "<<j->getPerso()->getPtsVie()<<"] [MANA : "<<j->getPerso()->getMana()<<"]"<<endl;
        cout <<"\n"<<endl;
        cout << " 1 - Jouer une carte " << endl;
        cout << " 2 - Utiliser une potion " << endl;
        cout << " 3 - Attaquer l'ennemi !" << endl;
        cout << " 4 - Afficher le plateau" << endl;
        cout << " 5 - Terminer le tour" << endl;
        cout << endl << "Choix : ";

        cin >> choix;
        switch (choix)
        {
        case 1:
            choisirCarte(j,d);
            break;
        case 2:
            choisirPotion(j,p,h);
            break;
        case 3:
            if(d->isEmpty()==true){
                cout <<"Vous n'avez pas de cartes sur le terrain"<<endl;
                system("pause");
                system("cls");
            }
            else
                menuCombat(j,d,p,boardennemi,joueurEnnemi,tour);
            break;
        case 4 :
            Plateau(j,joueurEnnemi,tour);
            break;
        case 5 :
            j->getPerso()->setManaPlus(3);
            jeuON = false;
            system("cls");
            break;
        default:
            cout << "Veuillez choisir un chiffre entre 1 et 5" <<endl;
            break;
        }

    }
}
void Jeu::initDeck(Joueur* j, Deck* d){
      for (int i=0;i<20;i++){
    j->getPioche()->ajouterCarte(d->prendreCarte());
  };
}
 void Jeu::choisirPerso(Joueur* j){
 int choix;
    bool jeuON = true;
    while (jeuON != false) {

        cout << endl <<"CHOISISSEZ VOTRE PERSONNAGE "<<j->getNom()<<" : "<<endl;
        cout << endl << " 1 - Le SHAMAN" << endl;
        cout <<"Rappel : 20 PTS VIE // 2 MANA" <<endl;
        cout << endl << " 2 - Le FOUDROYEUR " << endl;
        cout <<"Rappel : 10 PTS VIE // 4 MANA" <<endl;
        cout << endl << " 3 - LE SPADASSIN " << endl;
        cout <<"Rappel : 20 PTS VIE // 2 MANA" <<endl;
        cout << endl << " 4 - LE BERSERKER" << endl;
        cout <<"Rappel : 25 PTS VIE // 1 MANA" <<endl;
        cout << endl << "Choix du personnage : ";

        cin >> choix;

        switch (choix)
        {
        case 1:
            {
            Shaman* s = new Shaman(20,2,"Shaman");
            j->setPerso(s);
            jeuON = false;
            break;
            }
        case 2:
            {
            Foudroyeur* d = new Foudroyeur(10,4,"Foudroyeur");
            j->setPerso(d);
            jeuON = false;
            break;
            }
        case 3:
            {
            Spadassin* sp = new Spadassin(20,2,"Spadassin");
            j->setPerso(sp);
            jeuON = false;
            break;
            }
        case 4:
            {
            Berserker* b = new Berserker(25,1,"Berserker");
            j->setPerso(b);
            jeuON = false;
            break;
            }
        default:
            cout << "Veuillez choisir un chiffre entre 1 et 4";
            break;
        }

    }
    system("cls");
 }
void Jeu::choisirCarte(Joueur* j, Deck* d){
     int choix;
    bool select = true;
    while (select != false) {

            cout << endl <<"CHOISISSEZ LA CARTE QUE VOUS VOULEZ JOUER "<<j->getNom()<<" : "<<endl;
            cout << "\n" << endl;
            j->afficherHand();
            cout << "Choix de la carte : (0 = quitter le menu)" <<endl;
            cin >> choix;

        switch (choix)
        {
        case 0:
            {
            cout << "Vous passez votre tour" << endl;
            select = false;
            break;
            }
        case 1:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
            }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 2:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 3:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 4:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 5:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 6:
            {
            if(j->getPerso()->getMana()>=j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
            }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 7:
            {
            if(j->getPerso()->getMana() >= j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        case 8:
            {
            if(j->getPerso()->getMana() >= j->getHand()->selectCarteChoix(choix)->getCout()){
                j->getPerso()->setManaPlay(j->getHand()->selectCarteChoix(choix)->getCout());
                d->ajouterCarte(j->getHand()->prendreCarteChoix(choix));
                select = false;
                break;
                }
            else
                cout <<"Vous n'avez pas assez de mana"<<endl;
                system("pause");
                break;
            }
        default:
            cout << "Veuillez choisir un chiffre entre 1 et "<<j->getHand()->getSize()<<endl;
            break;
        }

    }
    system("cls");
}
