#include <iostream>
#include "Carte.h"
#include "Deck.h"
#include "Joueur.h"
#include "Personnage.h"
#include "Jeu.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include "windows.h"
using namespace std;


int main()
{
  HWND hwnd=GetForegroundWindow();
  ShowWindow(hwnd,SW_MAXIMIZE);
  Jeu* leJeu = new Jeu;
  cout  << "#######################################" <<endl;
  cout  << "##                                   ##" <<endl;
  cout  << "##          HEARTHSTONE LIKE         ##" <<endl;
  cout  << "##                                   ##" <<endl;
  cout  << "##         par Hugo de Almeida       ##" <<endl;
  cout  << "##                                   ##" <<endl;
  cout  << "#######################################" <<endl;
  leJeu->afficherMenu();
  leJeu->commencerJeu();
  return 0;

}
